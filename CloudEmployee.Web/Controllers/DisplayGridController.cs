﻿using CloudEmployee.Web.Models;
using CloudEmployee.Web.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CloudEmployee.Web.Controllers
{
    public class DisplayGridController : Controller
    {
        // GET: DisplayGrid
        public ActionResult Index()
        {
            SchoolService schoolService = new SchoolService();

            return View(schoolService.PopulateUser());
        }

        // GET: DisplayGrid/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: DisplayGrid/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: DisplayGrid/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: DisplayGrid/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: DisplayGrid/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: DisplayGrid/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: DisplayGrid/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
