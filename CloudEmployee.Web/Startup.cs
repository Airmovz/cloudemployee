﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CloudEmployee.Web.Startup))]
namespace CloudEmployee.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
