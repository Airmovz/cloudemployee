﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CloudEmployee.Web.Models
{
    public class School
    {
        public int Id { get; set; }
        public string SchoolName { get; set; }
        public string SchoolAbbr { get; set; }
        public int TotalEnrollee { get; set; }
        public DateTime DateEstablish{ get; set; }

        public School(int Id, string SchoolName, string SchoolAbbr, int TotalEnrollee, DateTime DateEstablish)
        {
            this.Id = Id;
            this.SchoolName = SchoolName;
            this.SchoolAbbr = SchoolAbbr;
            this.TotalEnrollee = TotalEnrollee;
            this.DateEstablish = DateEstablish;
        }
    }
}