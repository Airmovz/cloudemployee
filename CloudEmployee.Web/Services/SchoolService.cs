﻿using CloudEmployee.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudEmployee.Web.Services
{
    class SchoolService
    {
        public List<School> PopulateUser()
        {
            List<School> schoolList = new List<School>();
            
            schoolList = new List<School>();

            schoolList.Add(new School(1, "Cebu Institute of Medicine", "CIM", 10000, DateTime.Today));
            schoolList.Add(new School(2, "Cebu Institute of Technology - University", "CIT-U", 140, DateTime.Today));
            schoolList.Add(new School(3, "Cebu Normal University", "CNU", 140, DateTime.Today));
            schoolList.Add(new School(4, "Cebu Technological University", "CTU", 140, DateTime.Today));
            schoolList.Add(new School(5, "Southwestern University", "SWU", 140, DateTime.Today));
            schoolList.Add(new School(6, "University of Cebu", "UC", 140, DateTime.Today));
            schoolList.Add(new School(7, "University of San Carlos", "USC", 140, DateTime.Today));
            schoolList.Add(new School(8, "University of San Jose-Recoletos", "USJ-R", 140, DateTime.Today));
            schoolList.Add(new School(9, "University of Southern Philippines Foundation", "USPF", 140, DateTime.Today));
            schoolList.Add(new School(10, "University of the Visayas", "UV", 140, DateTime.Today));
            schoolList.Add(new School(11, "University of the Philippines", "UP", 140, DateTime.Today));
            schoolList.Add(new School(12, "University of the East", "UE", 140, DateTime.Today));
            schoolList.Add(new School(13, "University of Mindanao", "UM", 140, DateTime.Today));
            schoolList.Add(new School(14, "Cebu Doctors University", "CDU", 140, DateTime.Today));
            schoolList.Add(new School(15, "Adamson University", "AdU", 140, DateTime.Today));
            schoolList.Add(new School(16, "Arellano University", "AU", 140, DateTime.Today));
            schoolList.Add(new School(17, "College of the Holy Spirit", "CHS", 140, DateTime.Today));
            schoolList.Add(new School(18, "De La Salle University", "DLSU", 140, DateTime.Today));
            schoolList.Add(new School(19, "Emilio Aguinaldo College", "EAC", 140, DateTime.Today));
            schoolList.Add(new School(20, "Far Eastern University", "FEU", 140, DateTime.Today));
            schoolList.Add(new School(21, "Lyceum of the Philippines University", "LPU", 140, DateTime.Today));
            schoolList.Add(new School(22, "Mapua Institute of Technology", "MIT", 140, DateTime.Today));
            schoolList.Add(new School(23, "National University", "NU", 140, DateTime.Today));
            schoolList.Add(new School(24, "San Beda College", "SBC", 140, DateTime.Today));
            schoolList.Add(new School(25, "University of Santo Tomas", "UST", 140, DateTime.Today));

            return schoolList;
        }
    }
}
